package com.app.questions.sotalkie.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.questions.sotalkie.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity() {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */

    var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_login)
    }

    override fun onStart() {
        super.onStart()
        mAuth = FirebaseAuth.getInstance()
        sign_in_button.setOnClickListener { attemptRegister() }

        register_button.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_left
            )
        }
    }


    private fun attemptRegister() {
        resetError()

        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        var view: View? = null
        var cancel = false

        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.form_field_required)
            view = email
            cancel = true
        } else if (!isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_phone)
            view = email
            cancel = true
        }

        if (TextUtils.isEmpty(passwordStr)) {
            password.error = getString(R.string.form_field_required)
            view = password
            cancel = true
        } else if (!isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            view = password
            cancel = true
        }

        if (!cancel) {
            signIn()
        } else {
            view?.requestFocus()
        }
    }

    fun signIn() {
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()
        val auth = mAuth ?: return showSignInError()
        auth.signInWithEmailAndPassword(emailStr, passwordStr)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(
                        R.anim.slide_in_left,
                        R.anim.slide_out_left
                    )
                } else {
                    showSignInError()
                }
            }
    }

    fun resetError() {
        email.error = null
        password.error = null
    }

    fun showSignInError() {
        Toast.makeText(
            this, "Giriş başarısız.",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
