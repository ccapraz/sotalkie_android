package com.app.questions.sotalkie.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.questions.sotalkie.R
import kotlinx.android.synthetic.main.activity_who_am_igame_start.*


class WhoAmIGameStartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_who_am_igame_start)
        supportActionBar?.hide()
    }

    override fun onStart() {
        super.onStart()
        start_button.setOnClickListener {
            val intent = Intent(this, WhoAmIActivity::class.java)
            intent.putExtra("REFERENCE_CODE", ref_code.text.toString())

            startActivity(intent)
            overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_left
            )
        }
    }
}
