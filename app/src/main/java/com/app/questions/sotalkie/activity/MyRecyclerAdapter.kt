package com.app.questions.sotalkie.activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.questions.sotalkie.R
import kotlinx.android.synthetic.main.list_item.view.*

class MyRecyclerAdapter(var list: ArrayList<MyArray>) : RecyclerView.Adapter<MyRecyclerAdapter.MyHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyHolder {
        var v = LayoutInflater.from(p0.context).inflate(R.layout.list_item, p0, false)
        return MyHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: MyHolder, p1: Int) {
        var myArray = list.get(p1)
        p0.user_name.text = myArray.user_name
        p0.user_num.text = myArray.user_number
    }

    public class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view = itemView

        public var user_name = view.user_name
        public var user_num = view.user_number


    }


}