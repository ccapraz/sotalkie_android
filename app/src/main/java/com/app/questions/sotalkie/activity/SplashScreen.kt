package com.app.questions.sotalkie.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.app.questions.sotalkie.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_splash.*


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onStart() {
        super.onStart()
        hideActionBar()

        val slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up)
        slideUp.setAnimationListener(
            object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {}

                override fun onAnimationEnd(animation: Animation?) {
                    initializeFirebaseConnection()
                }

                override fun onAnimationStart(animation: Animation?) {}
            }
        )
        logo.startAnimation(slideUp)
        logo.visibility = View.VISIBLE
    }

    private fun hideActionBar() {
        supportActionBar?.hide()
    }

    private fun initializeFirebaseConnection() {
        FirebaseApp.initializeApp(this)
        val currentUser = FirebaseAuth.getInstance()?.currentUser
        showLoader()
        currentUser?.let {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_left
            )
        } ?: run {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            overridePendingTransition(
                R.anim.slide_in_left,
                R.anim.slide_out_left
            )
        }
    }

    private fun showLoader() {
        loader.visibility = View.VISIBLE
    }
}
