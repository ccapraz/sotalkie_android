package com.app.questions.sotalkie.activity

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.questions.sotalkie.R
import com.app.questions.sotalkie.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*


class RegisterActivity : AppCompatActivity() {

    var database: FirebaseDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_register)
    }

    override fun onStart() {
        super.onStart()
        database = FirebaseDatabase.getInstance()
        sign_in_button.setOnClickListener { attemptRegister() }

        birthdate.setOnClickListener {
            val mcurrentTime: Calendar = Calendar.getInstance()
            val year = mcurrentTime.get(Calendar.YEAR)
            val month = mcurrentTime.get(Calendar.MONTH)
            val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)

            val datePicker =
                DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    birthdate.setText("$dayOfMonth/$monthOfYear/$year")
                }, year, month, day)
            datePicker.setTitle("Tarih seçiniz.")
            datePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Ayarla", datePicker)
            datePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", datePicker)

            datePicker.show()
        }
    }


    private fun attemptRegister() {
        resetError()

        // Store values at the time of the login attempt.
        val nameStr = name.text.toString()
        val surnameStr = surname.text.toString()
        val emailStr = email.text.toString()
        val phoneStr = email.text.toString()
        val passwordStr = password.text.toString()
        val birthDateStr = birthdate.text.toString()

        var view: View? = null
        var cancel = false

        if (TextUtils.isEmpty(nameStr)) {
            name.error = getString(R.string.form_field_required)
            view = name
            cancel = true
        }

        if (TextUtils.isEmpty(surnameStr)) {
            surname.error = getString(R.string.form_field_required)
            view = surname
            cancel = true
        }

        if (TextUtils.isEmpty(phoneStr)) {
            email.error = getString(R.string.form_field_required)
            view = email
            cancel = true
        } else if (!isPhoneNumberValid(phoneStr)) {
            email.error = getString(R.string.error_invalid_phone)
            view = email
            cancel = true
        }

        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.form_field_required)
            view = email
            cancel = true
        } else if (!isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            view = email
            cancel = true
        }

        if (TextUtils.isEmpty(birthDateStr)) {
            birthdate.error = getString(R.string.form_field_required)
            view = birthdate
            cancel = true
        }

        if (TextUtils.isEmpty(passwordStr)) {
            password.error = getString(R.string.form_field_required)
            view = password
            cancel = true
        } else if (!isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            view = password
            cancel = true
        }

        if (!cancel) {
            registerUser()
        } else {
            view?.requestFocus()
        }
    }

    private fun registerUser() {
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailStr, passwordStr)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    setUserDetail()
                } else {
                    showRegisterError()
                }
            }
    }

    private fun setUserDetail() {
        val currentUser = FirebaseAuth.getInstance()?.currentUser ?: return showRegisterError()
        val db = database ?: return showRegisterError()
        val user = User(
            name = name.text.toString(),
            surname = surname.text.toString(),
            email = email.text.toString(),
            phone = email.text.toString(),
            birthdate = birthdate.text.toString()
        )

        db.getReference("users").child(currentUser.uid).setValue(user)

        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        overridePendingTransition(
            R.anim.slide_in_left,
            R.anim.slide_out_left
        )
    }

    fun showRegisterError() {
        Toast.makeText(
            this, "İşlem başarısız.",
            Toast.LENGTH_SHORT
        ).show()
    }

    fun resetError() {
        // Reset errors.
        name.error = null
        surname.error = null
        email.error = null
        email.error = null
        password.error = null
        birthdate.error = null
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }

    private fun isPhoneNumberValid(phone: String): Boolean {
        return android.util.Patterns.PHONE.matcher(phone).matches()
    }
}
