package com.app.questions.sotalkie.fragment


import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.app.questions.sotalkie.R
import com.app.questions.sotalkie.activity.ResultActivity
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_product_search.view.*

class ProductSearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_product_search, container, false)

        val list = listOf("Seminer Türü", "Sağlık", "Tadım", "Spor", "Meditasyon", "Kişisel gelişim", "İletişim")
        val list2 = listOf("Atölyeler", "Tadım", "Spor", "Meditasyon", "Sahne", "Müzik")
        val list3 = listOf(
            "Özel Günler",
            "Babalar Günü",
            "Anneler Günü",
            "Dünya Kadınlar Günü",
            "Dünya Su Günü",
            "Kanserle Savaş Haftası",
            "Dünya Sağlık Günü",
            "23 Nisan",
            "İş Güvenliği Haftası",
            "Dünya Çevre Günü",
            "Dünya Sigarayı Bırakma Günü"
        )
        val list4 = listOf("Mekan", "Outdoor", "Indoor")

        rootView.category_one.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, list)
        rootView.category_two.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, list2)
        rootView.category_three.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, list3)
        rootView.category_four.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, list4)

        rootView.my_button.setOnClickListener {
            rootView.search_field.hideKeyboard()
            val text: String = rootView.search_field.text.toString()
            val dialog: AlertDialog = SpotsDialog.Builder().setContext(context).build()

            dialog.show()
            val intent = Intent(context, ResultActivity::class.java)
            intent.putExtra("search", text)
            startActivity(intent)
            dialog.dismiss()
        }

        return rootView
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
