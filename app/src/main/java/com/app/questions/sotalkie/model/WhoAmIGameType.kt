package com.app.questions.sotalkie.model

enum class WhoAmIGameType {
    QUIZ, KNOW, INTRODUCE, FEEDBACK
}