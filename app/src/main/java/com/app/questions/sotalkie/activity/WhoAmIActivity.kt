package com.app.questions.sotalkie.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.questions.sotalkie.R
import com.app.questions.sotalkie.fragment.WaitingFragment
import com.app.questions.sotalkie.fragment.whoAmI.WhoAmIFeedbackFragment
import com.app.questions.sotalkie.fragment.whoAmI.WhoAmIIntroduceFragment
import com.app.questions.sotalkie.fragment.whoAmI.WhoAmIKnowFragment
import com.app.questions.sotalkie.fragment.whoAmI.WhoAmIQuizFragment
import com.app.questions.sotalkie.model.WhoAmIGameType
import com.app.questions.sotalkie.model.WhoAmIStatusModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class WhoAmIActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_who_am_i)
        supportActionBar?.hide()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, WaitingFragment())
        transaction.commit()
    }

    override fun onStart() {
        super.onStart()
        val ref_code = intent.getStringExtra("REFERENCE_CODE")
        Toast.makeText(this, ref_code, Toast.LENGTH_LONG).show()

        val gameListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                dataSnapshot.getValue(WhoAmIStatusModel::class.java)?.let {
                    manageGameWtatus(it)
                } ?: run {
                    Toast.makeText(applicationContext, "Referans Kodu Bulunamadı", Toast.LENGTH_LONG).show()
                    val intent = Intent(applicationContext, WhoAmIGameStartActivity::class.java)

                    startActivity(intent)
                    overridePendingTransition(
                        R.anim.slide_in_left,
                        R.anim.slide_out_left
                    )
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                println("loadPost:onCancelled ${databaseError.toException()}")
            }
        }

        FirebaseDatabase.getInstance().getReference("who_am_i").child(ref_code).child("status")
            .addValueEventListener(gameListener)
    }

    fun manageGameWtatus(whoAmIStaus: WhoAmIStatusModel) {
        val mAuth = FirebaseAuth.getInstance()
        val isCurrentCustomerTurn = mAuth.currentUser?.uid == whoAmIStaus.currentUserId

        when (whoAmIStaus.gameType) {
            WhoAmIGameType.FEEDBACK -> {
                val transaction = supportFragmentManager.beginTransaction()
                if (isCurrentCustomerTurn) {
                    transaction.replace(R.id.fragment_container, WaitingFragment())
                } else {
                    transaction.replace(R.id.fragment_container, WhoAmIFeedbackFragment())
                }
                transaction.commit()
            }
            WhoAmIGameType.QUIZ -> {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.fragment_container, WhoAmIQuizFragment())
                transaction.commit()
            }
            WhoAmIGameType.KNOW -> {
                val transaction = supportFragmentManager.beginTransaction()
                if (isCurrentCustomerTurn) {
                    transaction.replace(R.id.fragment_container, WaitingFragment())
                } else {
                    transaction.replace(R.id.fragment_container, WhoAmIKnowFragment())
                }
                transaction.commit()
            }
            WhoAmIGameType.INTRODUCE -> {
                val transaction = supportFragmentManager.beginTransaction()
                if (isCurrentCustomerTurn) {
                    transaction.replace(R.id.fragment_container, WaitingFragment())
                } else {
                    transaction.replace(R.id.fragment_container, WhoAmIIntroduceFragment())
                }
                transaction.commit()
            }
        }

    }
}
