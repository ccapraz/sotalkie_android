package com.app.questions.sotalkie.model

data class WhoAmIStatusModel(
    val gameType: WhoAmIGameType,
    val currentUserId: String,
    val question: String,
    val options: List<String>?
)