package com.app.questions.sotalkie.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.questions.sotalkie.R
import com.app.questions.sotalkie.fragment.ProductFragment
import com.app.questions.sotalkie.fragment.ProductSearchFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class HomeActivity : AppCompatActivity() {

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        val transaction = supportFragmentManager.beginTransaction()

        when (item.itemId) {
            R.id.navigation_home -> {
                transaction.replace(R.id.fragment_container, ProductFragment())
                transaction.addToBackStack(null)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                transaction.replace(R.id.fragment_container, ProductSearchFragment())
                transaction.addToBackStack(null)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            else -> {
                transaction.replace(R.id.fragment_container, ProductFragment())
                transaction.addToBackStack(null)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        supportActionBar?.hide()

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, ProductFragment())
        transaction.commit()
    }
}
