package com.app.questions.sotalkie.activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.questions.sotalkie.R
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val text = intent.getStringExtra("search")

        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#9b2ba5")))
        if (text.isBlank()) {
            supportActionBar?.title = "Arama sonuçları"
        } else {
            supportActionBar?.title = intent.getStringExtra("search") + " için sonuçlar"
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val temp = ArrayList<MyArray>()
        temp.add(MyArray("Sigara Bırakma", "ABC Holding"))
        temp.add(MyArray("Yemek Pişirme", "DEF Ajansı"))
        temp.add(MyArray("Takla Atma", "Macfit"))
        temp.add(MyArray("Kalem çevirme kursu", "ergence.org"))
        temp.add(MyArray("İngilizce Eğitim", "Amerikan Kültür Derneği"))
        temp.add(MyArray("Birimler arası bilgi yarışması", "bilgili.org"))

        val myData = temp.filter {
            it.user_name.toLowerCase().contains(text) || it.user_number.toLowerCase().contains(text.toLowerCase())
        }

        results_recycler_view.layoutManager = LinearLayoutManager(this)
        results_recycler_view.adapter = MyRecyclerAdapter(ArrayList(myData))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
