package com.app.questions.sotalkie.model

data class User (
    val name: String,
    val surname: String,
    val email: String,
    val phone: String,
    val birthdate: String
)